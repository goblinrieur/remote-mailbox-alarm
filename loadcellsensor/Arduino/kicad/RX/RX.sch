EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "mailbox notification RX side"
Date "2021-08-28"
Rev "0.1.0"
Comp ""
Comment1 "Free to reuse "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Buzzer BZ1
U 1 1 612A7059
P 9100 2950
F 0 "BZ1" H 9252 2979 50  0000 L CNN
F 1 "Buzzer" H 9252 2888 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" V 9075 3050 50  0001 C CNN
F 3 "~" V 9075 3050 50  0001 C CNN
	1    9100 2950
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 612A81A4
P 3000 2250
F 0 "D1" V 3039 2132 50  0000 R CNN
F 1 "LED_red" V 2948 2132 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 3000 2250 50  0001 C CNN
F 3 "~" H 3000 2250 50  0001 C CNN
	1    3000 2250
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:2N3904 Q1
U 1 1 612A9F98
P 3100 1850
F 0 "Q1" H 3291 1896 50  0000 L CNN
F 1 "2N3904" H 3291 1805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3300 1775 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 3100 1850 50  0001 L CNN
	1    3100 1850
	-1   0    0    -1  
$EndComp
Text GLabel 3400 1850 2    50   Input ~ 0
alarm_letters
$Comp
L power:+5V #PWR01
U 1 1 612ADAA1
P 3000 1600
F 0 "#PWR01" H 3000 1450 50  0001 C CNN
F 1 "+5V" H 3015 1773 50  0000 C CNN
F 2 "" H 3000 1600 50  0001 C CNN
F 3 "" H 3000 1600 50  0001 C CNN
	1    3000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1600 3000 1650
Wire Wire Line
	3000 2100 3000 2050
Wire Wire Line
	3300 1850 3400 1850
$Comp
L Device:R_US R1
U 1 1 612B09AC
P 3000 2700
F 0 "R1" H 3068 2746 50  0000 L CNN
F 1 "220" H 3068 2655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3040 2690 50  0001 C CNN
F 3 "~" H 3000 2700 50  0001 C CNN
	1    3000 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2400 3000 2500
$Comp
L power:GND #PWR02
U 1 1 612B1F19
P 3000 2950
F 0 "#PWR02" H 3000 2700 50  0001 C CNN
F 1 "GND" H 3005 2777 50  0000 C CNN
F 2 "" H 3000 2950 50  0001 C CNN
F 3 "" H 3000 2950 50  0001 C CNN
	1    3000 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 2850 3000 2950
$Comp
L Device:LED D2
U 1 1 612C458D
P 5000 2250
F 0 "D2" V 5039 2132 50  0000 R CNN
F 1 "LED_blue" V 4948 2132 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5000 2250 50  0001 C CNN
F 3 "~" H 5000 2250 50  0001 C CNN
	1    5000 2250
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:2N3904 Q3
U 1 1 612C4593
P 5100 1850
F 0 "Q3" H 5291 1896 50  0000 L CNN
F 1 "2N3904" H 5291 1805 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 5300 1775 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 5100 1850 50  0001 L CNN
	1    5100 1850
	-1   0    0    -1  
$EndComp
Text GLabel 5400 1850 2    50   Input ~ 0
alarm_packages
$Comp
L power:+5V #PWR03
U 1 1 612C459A
P 5000 1600
F 0 "#PWR03" H 5000 1450 50  0001 C CNN
F 1 "+5V" H 5015 1773 50  0000 C CNN
F 2 "" H 5000 1600 50  0001 C CNN
F 3 "" H 5000 1600 50  0001 C CNN
	1    5000 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 1600 5000 1650
Wire Wire Line
	5000 2100 5000 2050
Wire Wire Line
	5300 1850 5400 1850
$Comp
L Device:R_US R2
U 1 1 612C45A3
P 5000 2700
F 0 "R2" H 5068 2746 50  0000 L CNN
F 1 "220" H 5068 2655 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5040 2690 50  0001 C CNN
F 3 "~" H 5000 2700 50  0001 C CNN
	1    5000 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2400 5000 2500
$Comp
L power:GND #PWR04
U 1 1 612C45AA
P 5000 2950
F 0 "#PWR04" H 5000 2700 50  0001 C CNN
F 1 "GND" H 5005 2777 50  0000 C CNN
F 2 "" H 5000 2950 50  0001 C CNN
F 3 "" H 5000 2950 50  0001 C CNN
	1    5000 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2850 5000 2950
$Comp
L Transistor_BJT:2N3904 Q2
U 1 1 612AE8DC
P 9100 2500
F 0 "Q2" H 9291 2546 50  0000 L CNN
F 1 "2N3904" H 9291 2455 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 9300 2425 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/2N3903-D.PDF" H 9100 2500 50  0001 L CNN
	1    9100 2500
	-1   0    0    -1  
$EndComp
Text GLabel 5450 2500 2    50   Input ~ 0
sound
Text GLabel 3350 2500 2    50   Input ~ 0
sound
Wire Wire Line
	5450 2500 5000 2500
Connection ~ 5000 2500
Wire Wire Line
	5000 2500 5000 2550
Wire Wire Line
	3350 2500 3000 2500
Connection ~ 3000 2500
Wire Wire Line
	3000 2500 3000 2550
$Comp
L power:+5V #PWR05
U 1 1 612C5EED
P 9000 2250
F 0 "#PWR05" H 9000 2100 50  0001 C CNN
F 1 "+5V" H 9015 2423 50  0000 C CNN
F 2 "" H 9000 2250 50  0001 C CNN
F 3 "" H 9000 2250 50  0001 C CNN
	1    9000 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 2250 9000 2300
Text GLabel 9900 2500 2    50   Input ~ 0
sound
$Comp
L Switch:SW_DPST_x2 SW1
U 1 1 612C8300
P 9600 2500
F 0 "SW1" H 9600 2735 50  0000 C CNN
F 1 "SW_DPST_x2" H 9600 2644 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9600 2500 50  0001 C CNN
F 3 "~" H 9600 2500 50  0001 C CNN
	1    9600 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 2500 9800 2500
Wire Wire Line
	9400 2500 9300 2500
Wire Wire Line
	9000 2700 9000 2850
$Comp
L Device:R_US R3
U 1 1 612C92E4
P 9000 3350
F 0 "R3" H 9068 3396 50  0000 L CNN
F 1 "100" H 9068 3305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 9040 3340 50  0001 C CNN
F 3 "~" H 9000 3350 50  0001 C CNN
	1    9000 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 3050 9000 3200
$Comp
L power:GND #PWR06
U 1 1 612C9722
P 9000 3600
F 0 "#PWR06" H 9000 3350 50  0001 C CNN
F 1 "GND" H 9005 3427 50  0000 C CNN
F 2 "" H 9000 3600 50  0001 C CNN
F 3 "" H 9000 3600 50  0001 C CNN
	1    9000 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 3500 9000 3600
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 612D414E
P 3400 4900
F 0 "J1" H 3428 4876 50  0000 L CNN
F 1 "RX433MHz" H 3428 4785 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x04_P2.54mm_Vertical" H 3400 4900 50  0001 C CNN
F 3 "~" H 3400 4900 50  0001 C CNN
	1    3400 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 612D499D
P 3000 5250
F 0 "#PWR08" H 3000 5000 50  0001 C CNN
F 1 "GND" H 3005 5077 50  0000 C CNN
F 2 "" H 3000 5250 50  0001 C CNN
F 3 "" H 3000 5250 50  0001 C CNN
	1    3000 5250
	1    0    0    -1  
$EndComp
Text GLabel 3000 4900 0    50   Input ~ 0
RX_DATA
Wire Wire Line
	3000 4900 3100 4900
Wire Wire Line
	3100 4900 3100 5000
Wire Wire Line
	3100 5000 3200 5000
Connection ~ 3100 4900
Wire Wire Line
	3100 4900 3200 4900
Wire Wire Line
	3000 5100 3000 5250
Wire Wire Line
	3000 5100 3200 5100
$Comp
L power:+5V #PWR07
U 1 1 612D7CBC
P 3000 4650
F 0 "#PWR07" H 3000 4500 50  0001 C CNN
F 1 "+5V" H 3015 4823 50  0000 C CNN
F 2 "" H 3000 4650 50  0001 C CNN
F 3 "" H 3000 4650 50  0001 C CNN
	1    3000 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4650 3000 4800
Wire Wire Line
	3000 4800 3200 4800
Text GLabel 6500 4850 0    50   Input ~ 0
alarm_letters
Text GLabel 6500 4950 0    50   Input ~ 0
alarm_packages
Text GLabel 6500 5050 0    50   Input ~ 0
RX_DATA
$Comp
L Connector:Conn_01x05_Female J2
U 1 1 612E74DD
P 6800 4950
F 0 "J2" H 6828 4976 50  0000 L CNN
F 1 "to_µC" H 6828 4885 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x05_P2.54mm_Vertical" H 6800 4950 50  0001 C CNN
F 3 "~" H 6800 4950 50  0001 C CNN
	1    6800 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR09
U 1 1 612E8080
P 6500 4650
F 0 "#PWR09" H 6500 4500 50  0001 C CNN
F 1 "+5V" H 6515 4823 50  0000 C CNN
F 2 "" H 6500 4650 50  0001 C CNN
F 3 "" H 6500 4650 50  0001 C CNN
	1    6500 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 612E860A
P 6500 5250
F 0 "#PWR010" H 6500 5000 50  0001 C CNN
F 1 "GND" H 6505 5077 50  0000 C CNN
F 2 "" H 6500 5250 50  0001 C CNN
F 3 "" H 6500 5250 50  0001 C CNN
	1    6500 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 4650 6500 4750
Wire Wire Line
	6500 4750 6600 4750
Wire Wire Line
	6600 4850 6500 4850
Wire Wire Line
	6500 4950 6600 4950
Wire Wire Line
	6600 5050 6500 5050
Wire Wire Line
	6600 5150 6500 5150
Wire Wire Line
	6500 5150 6500 5250
Text Notes 1500 7150 0    50   ~ 0
RX SIDE \n\nAll in that side, is directly connected to a dev board whatever the model is.\n\nSo Using only connectors & passive componants is enough for that function.\n\nRX_DATA is read, µC determinates what alarm to rise up.\n\nUser can use µC devboard reset button, to stop alarm. He can also disable/enable sound with interrupor.\n
$EndSCHEMATC
