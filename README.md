# remote-mailbox-alarm

Detect & rise up a flashing light & activate a buzzer in house or apartment if a new mail or package is present in the mailbox.

System must be quite cheap, & reset-able when user has grabbed his stuff from the mailbox.

System must also not consume batteries too fast in the mailbox side. _house side can be on sector or USB power & so on_

Flashing light might use a high light led between 5 & 10 millimeters.

Buzzer can be as simple as an active buzzer in parallel to the flashing light.

# the Idea is known by many many people

But I wanna make mine from a DIY solution, & adapt it to my own needs & preferences.

I got information from another user similar project, but I will simplify it to my own needs.

**Reboot project** : sam. 28 August 2021 15:54:50 CEST : Changed all of the concept idea to a better choice

# concept

- [X] Get all needed parts in stock

- [X] 2x Arduino Pro micro

- [X] Purchased Super capacitors

- [X] Resistors

- [X] Transistors 2N3904

- [X] Connectors & wires

- [X] 433MHz modules _emitter & receiver_

- [X] buzzer _might be disabled from a switch_

- [X] Push buttons & switches

- [X] Good lighting quality LED's

- [X] Mini Voltmeter 

Something is put in mailbox, its weight is send as a message using 433MHz module

Its weight determinate _(from probabilities)_ what king of object it is _(letter(s), adds, little package, bigger package)_

![diagram](./UseCaseDiagram.png)

# needs 

- [ ] use sleep mode to consume low current from battery

- [ ] use weight to identify little package vs bigger package vs letter vs few letters

- [ ] load cell sensors will be used for that

## How to

- [ ] triangle form factor of 3 sensors  _(have same size & sensibility, 2 of them will wake up the micro-controller & emitter)_

- [ ] power up from transistor the emitter

- [ ] then they are measured in grams units 

- [ ] emit signal in loop for 1 minute to be sure it will be received

## Receiver (code & hardware)

Receiver side, concept doesn't really change, might just be optimized & adding new functions 

- [X] Hardware designed compatible to any micro-controller

- [ ] Code

## Emitter (code & hardware)

- [ ] first state is power it from a lipo 7.3v lipo 

- [ ] on sensor detection on mail wake up micro-controller

- [ ] micro-controller rise up with transistor emitter power mode _(to charge a super-capacitor)_

- [ ] measure weight

- [ ] wait some hundreds of micro seconds

- [ ] stop charging the capacitor _(transistor stop mode)_

- [ ] make emitter module up from another transistor _(powered by the super capacitor now)_

- [ ] loop emission until 1 minute

- [ ] make micro-controller goes back to sleeping mode.

- [ ] special **hardware function** : _user can check battery level from a very simple voltage measure module, activated from a push button, to know if he needs to recharge battery_ 

- [ ] code

# TODO 

- [ ] prototype charging super capacitor & switching to it as main power of emitter module

- [ ] prototype emitter

- [ ] prototype receiver

- [ ] try communication between them

- [ ] adjust code & hardware to get the perfect settings & sensibility

- [ ] Make it all on a prototype board

- [ ] Try it all in real conditions & if all is OK, then make PCB from JLCPCB 

- [ ] As option make both project versions, Arduino based one, ATtiny based one.

# How to reuse this project for yourself 

- fork the project

- choose your license but it might be MIT see [LICENSE](./LISCENSE) file

- use a directory tree as detailed & standardized as possible see my example just below :

```
.
└── loadcellsensor
    ├── BOX
    │   ├── RX
    │   └── TX
    ├── code
    │   ├── RX
    │   └── TX
    └── kicad
        ├── RX
        └── TX
```

Fist directory is main technical concept used, below separate code, box & chosen electronic design tool

Below that use as many sub parts as needed _(here RX and TX)_
